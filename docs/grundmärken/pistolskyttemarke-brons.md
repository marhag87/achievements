# Pistolskyttemärke Brons
- [x] 3 precisionsserier, 34 poäng
- [x] En av följande:
    * [x] 3 tillämpningsserier, 5 träff, 60 sek
    * [ ] Standardmedalj fält

![Screenshot](img/Pistolskyttemarke_brons.png){: style="height:250px;width:250px"}
