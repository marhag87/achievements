# Pistolskyttemärke Silver
- [x] 3 precisionsserier, 40 poäng
- [x] En av följande:
    * [ ] 3 tillämpningsserier, 6 träff, 40 sek
    * [x] Standardmedalj fält

![Screenshot](img/Pistolskyttemarke_silver.png){: style="height:250px;width:250px"}
