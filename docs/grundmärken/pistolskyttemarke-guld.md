# Pistolskyttemärke Guld
- [x] 3 precisionsserier, 46 poäng
- [x] En av följande:
    * [ ] 3 tillämpningsserier, 6 träff, 15 sek
    * [x] Standardmedalj fält

![Screenshot](img/Pistolskyttemarke_guld.png){: style="height:250px;width:250px"}
